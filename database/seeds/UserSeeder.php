<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([
            'name'=>'admin',
            'email'=>'admin@bncc.net',
            'password'=>Hash::make('VIVABNCC'),
            'phone'=>'admin',
            'lineid'=>'admin',
            'jurusan'=>'admin',
            'NIM'=>'000',
            'role_id'=>1,
        ]);
    }
}
