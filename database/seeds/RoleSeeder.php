<?php

use App\Role;
use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::insert($this->roles());
    }
    public function roles(){
        return [
            ['id' => '1',
            'role' => 'admin'],
            ['id' => '2',
            'role'=> 'participant'],
            ['id' => '3',
            'role' => 'binusian'],
            ['id' => '4',
            'role' => 'OK']
        ];
    }
}
