<?php

use Illuminate\Database\Seeder;
class SeminarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Seminar::create([
            'title' =>'Devcamp',
            'speaker' =>'Dr. Devcamp',
            'date' =>'2020-08-20',
            'time' =>'13:20:00',
            'link' =>'https://bncc.net',
        ]);
    }
}
