<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'

], function () {
    Route::post('login', 'AuthController@login')->name('login');
    Route::post('register', 'AuthController@register')->name('register');
    Route::post('logout', 'AuthController@logout')->name('logout');
    Route::post('refresh', 'AuthController@refresh')->name('refresh');

    Route::group(['middleware' => 'auth'], function () {
        // User Profile
        Route::get('profile', 'AuthController@userProfile');
        //payment
        Route::post('/payment', 'PaymentController@createPayment');
        Route::post('/payments/{payment_id}','PaymentController@updatePayment');
        Route::delete('/payments/{payment_id}','PaymentController@deletePayment');
        Route::get('/payment','PaymentController@getPaymentById')->name('viewPersonalPayment');
        // Input data Binus NIM
        Route::post('NIM','AuthController@inputNIM')->name('inputNim');

        // Admin
        Route::group(['middleware' => 'admin'], function () {
            Route::get('/admin','AdminController@getAllUser')->name('admin');
            Route::post('admin/{user_id}','AdminController@updateUser')->name('updateUser');
            Route::delete('admin/{user_id}','AdminController@deleteUser')->name('deleteUser');

            Route::post('admin/flazz/{user_id}','AuthController@updateFlazz')->name('updateFlazz');

            // Verified Binusian
            Route::post('verified/{user_id}','AdminController@verifyBinusian')->name('verfiedBinusian');
            // verfied Payment
            Route::get('/view-payments','PaymentController@getAllPayment');
            Route::post('payment_verified/{user_id}','AdminController@verifyPayment')->name('verfiedPayment');
            //seminar
            Route::get('/admin/seminars','SeminarController@getAllSeminars')->name('viewSeminar');
            Route::post('/seminar','SeminarController@createSeminar')->name('storeSeminar');
            Route::post('/seminar/{seminar_id}','SeminarController@updateSeminar')->name('updateSeminar');
            Route::delete('/seminar/{seminar_id}','SeminarController@deleteSeminar')->name('deleteSeminar');
            // view Flazz
            Route::get('user/{user_id}','AuthController@getUserById')->name('viewFlazz');
            // View ContactUs Message
            Route::get('contact','SendMessageController@getAllMessages')->name('viewMessage');
            // Email
            Route::post('email','SeminarController@sendMail')->name('reminder');
            Route::post('password/email','AdminController@forgot')->name('ResetPass');
            Route::post('password/reset', 'AdminController@reset');
        });
        Route::group(['middleware' => 'payment'], function () {
            Route::get('/seminars','SeminarController@getAllSeminars')->name('viewSeminarLink');
        });
    });
});

//send message
Route::post('contact/create','SendMessageController@createMessage')->name('storeMessage');

