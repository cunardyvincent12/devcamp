<?php

namespace App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserDataRequest;
use App\Payment;
use App\Repositories\AuthRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use App\User;

class AuthenticateService {

    protected $authRepository;

    public function __construct(AuthRepository $authRepository, UserRepository $userRepository)
    {
        $this->authRepository = $authRepository;
        $this->userRepository = $userRepository;
    }

    public function login(Request $request){
    	$validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (! $token = auth()->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->createNewToken($token);
    }

    public function register(UserDataRequest $request) {
        $fileName = $request->name;
        $file = $request->has('flazz') ? $this->uploadImage($request->file('flazz'), $fileName) : null;
        $attributes = [
            'name' => $request->name,
            'email'=>$request->email,
            'password'=>bcrypt($request->password),
            'phone'=>$request->phone,
            'lineid'=>$request->lineid,
            'flazz' => $file,
            'role_id'=>2
        ];
        return $this->authRepository->createUser($attributes);
    }
    public function updateFlazz($paymentID, Request $request)
    {
        $payment = $this->userRepository->getUserByID($paymentID);;
        $fileName = $payment['flazz'];
        Storage::delete('public/users/'.$fileName);
        $file = $request->has('flazz') ? $this->uploadImage($request->file('flazz'), $request->name) : null;

        // File
        $attributes = [
            'id' => $paymentID,
            'flazz' => $file
        ];
        return $this->userRepository->updateUser($paymentID, $attributes);
    }
    public function getUserById($userID){
        $user = $this->authRepository->getUserById($userID)->select('flazz')->find($userID);
        return $user;
    }

    public function logout() {
        auth()->logout();

        return response()->json(['message' => 'User successfully signed out']);
    }

    public function refresh() {
        return $this->createNewToken(auth()->refresh());
    }

    public function userProfile() {
        return response()->json(auth()->user());
    }

    public function uploadImage($data, $fileName)
    {
        $extension = $data->getClientOriginalExtension();
        $fileWithExtension = $fileName ."_". time() .'.' . $extension;
        $path = $data->storeAs('users', $fileWithExtension,'s3');
        return $path;
    }

    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => $this->authRepository->getUserByID(auth()->user()->id)->first(),
        ]);
    }

}
