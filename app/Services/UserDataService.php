<?php

namespace App\Services;
use Illuminate\Http\Request;
use App\Repositories\UserRepository;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserDataService {

    protected $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getAllUsers($userData)
    {
        return $this->userRepository->getAllUsers($userData);
    }

    public function getUserByID($userID)
    {
        return $this->userRepository->getUserByID($userID);
    }

    public function updateUser($userID, Request $request)
    {

        $this->userRepository->getUserByID($userID);
        $userData = [
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=> $request->phone,
            'lineid'=>$request->lineid,
            'jurusan'=>$request->jurusan,
            'NIM'=>$request->NIM
        ];
        return $this->userRepository->updateUser($userID, $userData);
    }
    public function updateUserBinus(Request $request)
    {
        $userID = JWTAuth::parseToken()->authenticate()->id;
        $this->userRepository->getUserByID($userID);
        $userData = [
                'jurusan'=>$request->jurusan,
                'NIM'=>$request->NIM
        ];
        return $this->userRepository->updateUserBinus($userID, $userData);
    }

    public function deleteUser($userID)
    {
        return $this->userRepository->deleteUser($userID);
    }

    public function verifiedBinusian($userID)
    {
        $data = $this->userRepository->getUserByID($userID);
        $userData = [
            'name'=>$data->name,
            'email'=>$data->email,
            'phone'=> $data->phone,
            'lineid'=>$data->lineid,
            'Binusian'=>[
                'jurusan'=>$data->jurusan,
                'NIM'=>$data->NIM
            ],
            'role_id'=> 3
        ];
        return $this->userRepository->updateUser($userID, $userData);

    }
}
