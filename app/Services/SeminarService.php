<?php

namespace App\Services;

use App\Jobs\SendLink;
use App\Mail\VideoConferenceLink;
use App\Payment;
use Illuminate\Http\Request;
use App\Repositories\SeminarRepository;
use App\Repositories\UserRepository;
use App\User;

class SeminarService {

    protected $seminarRepository;

    public function __construct(SeminarRepository $seminarRepository, UserRepository $userRepository)
    {
        $this->seminarRepository = $seminarRepository;
        $this->userRepository = $userRepository;
    }

    public function getAllSeminars($seminarData)
    {
        return $this->seminarRepository->getAllSeminars($seminarData);
    }

    public function createSeminar(Request $request)
    {
        $seminarData = [
            'title'=>$request->title,
            'speaker'=>$request->speaker,
            'date'=> $request->date,
            'time'=>$request->time,
            'link'=>$request->link
        ];
        return $this->seminarRepository->createSeminar($seminarData);
    }

    public function updateSeminar(Request $request, $seminarID)
    {
        $seminar = $this->seminarRepository->getSeminarByID($seminarID);
        $seminarData = [
            'title'=>$request->title,
            'speaker'=>$request->speaker,
            'date'=> $request->date,
            'time'=>$request->time,
            'link'=>$request->link
        ];
        return $this->seminarRepository->updateSeminar($seminarID, $seminarData);
    }

    public function deleteSeminar($seminarID)
    {
        return $this->seminarRepository->deleteSeminar($seminarID);
    }

    public function sendMail()
    {
        $datas = $this->userRepository->getAllUsers();
        foreach($datas as $data)
        {
           if(!array_key_exists('payment_id',$data)){
                $payment_status = Payment::where('id',$data['Payment']['id'])->value('status');
                if($payment_status == 1 ){
                    dispatch(new SendLink($data));
                }
           }
        }
        // dd(env('MAIL_USERNAME'),env('APP_NAME'));
    }
    public function paymentNotification(){

    }
}
