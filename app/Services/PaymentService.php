<?php

namespace App\Services;
use Illuminate\Http\Request;
use App\Http\Requests\PaymentRequest;
use App\Payment;
use App\Repositories\PaymentRepository;
use App\Repositories\UserRepository;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class PaymentService {

    protected $paymentRepository;

    public function __construct(PaymentRepository $paymentRepository, UserRepository $userRepository)
    {
        $this->paymentRepository = $paymentRepository;
        $this->userRepository = $userRepository;
    }

    public function getAllPayments()
    {
        return $this->paymentRepository->getAllPayments();
    }

    public function getPaymentByID($paymentID)
    {
        return $this->paymentRepository->getPaymentByID($paymentID);
    }

    public function createPayment(Request $request)
    {
        // dd(auth()->check());
        $fileName = auth()->user()->name.'_'.'Payment';
        $file = $request->has('image') ? $this->uploadImage($request->file('image'), $fileName) : null;
        $attributes = [
            'name' => $fileName,
            'image' => $file,
        ];
        $payment_id = ['payment_id'=>$this->paymentRepository->createPayment($attributes)->id];
        $this->userRepository->updateUser(auth()->user()->id,$payment_id);
        return $this->paymentRepository->getPaymentByID($payment_id);
    }

    public function deletePayment($paymentID)
    {
        return $this->paymentRepository->deletePayment($paymentID);
    }

    public function updatePayment($paymentID, PaymentRequest $request)
    {
        $payment = $this->paymentRepository->getPaymentByID($paymentID);;
        $fileName = $payment['image'];
        Storage::delete('public/payments/'.$fileName);
        $file = $request->has('image') ? $this->uploadImage($request->file('image'), $request->name) : null;

        // File
        $attributes = [
            'name' => $request->name,
            'image' => $file
        ];
        return $this->paymentRepository->updatePaymentData($paymentID, $attributes);
    }

    public function uploadImage($data, $fileName)
    {
    // dd($data,$fileName);
        $extension = $data->getClientOriginalExtension();
        $fileWithExtension = $fileName ."_". time() .'.' . $extension;
        $path = $data->storeAs('payments',$fileWithExtension,'s3');
        return $path;
    }
    public function verfiedPayment($userID){

        $payment_id = $this->userRepository->getUserById($userID)->find($userID)->payment_id;
        $data = $this->paymentRepository->getPaymentByID($payment_id);
        $attribut = [
            'name' => $data->name,
            'image' => $data->image ,
            'status' => 1
        ];
        return $this->paymentRepository->updatePaymentData($payment_id,$attribut);
    }
}
