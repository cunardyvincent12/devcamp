<?php

namespace App\Services;
use Illuminate\Http\Request;
use App\Repositories\SendMessageRepository;

class SendMessageService {

    protected $messageRepository;

    public function __construct(SendMessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
    }

    public function getAllMessages(Request $request)
    {
        return $this->messageRepository->getAllMessages($request->page);
    }

    public function createMessage(Request $request)
    {
        $messageData = [
            'name'=>$request->name,
            'email'=>$request->email,
            'subject'=>$request->subject,
            'message'=>$request->message
        ];
        return $this->messageRepository->createMessage($messageData);
    }

    public function googleReCaptcha(){
        
    }
}
