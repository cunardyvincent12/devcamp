<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    protected $fillable = [
        'name','image', 'status'
    ];

    public function users()
    {
        return $this->hasOne('App\User','payment_id');
    }
}
