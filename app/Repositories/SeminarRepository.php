<?php

namespace App\Repositories;

use App\Seminar;
use Illuminate\Support\Facades\DB;

class SeminarRepository{
    protected $seminar;

    public function __construct(Seminar $seminar)
    {
        $this->seminar = $seminar;
    }

    public function getAllSeminars()
    {
        $seminarData = DB::table('seminars')
        ->select('id','title','speaker','date','time','link')
        ->get();
        return $seminarData->transform(function($item){
            return [
                'id'=>$item->id,
                'title'=>$item->title,
                'speaker'=>$item->speaker,
                'schedule'=>[
                    'date'=>$item->date,
                    'time'=>$item->time
                ],
                'link'=>$item->link
            ];
        });
    }
    public function getSeminarByID($seminarID)
    {
        return $this->seminar->find($seminarID);
    }
    public function createSeminar($seminarData)
    {
        return $this->seminar->create($seminarData);
    }
    public function updateSeminar($seminarID, $seminarData)
    {
        return $this->seminar->find($seminarID)->update($seminarData);
    }
    public function deleteSeminar($seminarID)
    {
        return $this->seminar->find($seminarID)->delete();
    }

}
