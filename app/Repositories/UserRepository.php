<?php

namespace App\Repositories;

use App\Role;
use App\User;
use Illuminate\Support\Facades\DB;

class UserRepository{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getAllUsers()
    {
        $first = DB::table('users')->whereNull('payment_id')
        ->select('id','name as userName','password','email','flazz','phone','lineid','NIM','jurusan','role_id','payment_id')
        ->get();
        $userData = DB::table('users')->join('payments', 'users.payment_id', '=', 'payments.id')
        ->select('users.id','password','users.name as userName','email','flazz','phone','lineid','jurusan','NIM','role_id','payment_id','payments.name as paymentName','payments.image','payments.status')
        ->get();
        // dd($first);
        $merge =$this->userResponseStucture($first);
        $merge_data=$this->userResponseStucture($userData);
        return array_merge($merge,$merge_data);
    }
    public function userResponseStucture($item){
        $user =[];
        foreach($item as $data){
            $flazz = ($data->flazz == "") ? NULL : 'https://virtualconference-app.s3-ap-southeast-1.amazonaws.com/'.$data->flazz;
            if($data->payment_id == NULL){
               array_push($user,[
                    'id'=>$data->id,
                    'name'=>$data->userName,
                    'email'=>$data->email,
                    'phone'=>$data->phone,
                    'lineid'=>$data->lineid,
                    'Binusian'=>[
                        'jurusan'=>$data->jurusan,
                        'NIM'=>$data->NIM,
                        'flazz'=>$flazz,
                    ],
                    'role_id'=>$data->role_id,
                    'payment_id'=>$data->payment_id
                ]);
            }
            else{
                array_push($user, [
                    'id'=>$data->id,
                    'name'=>$data->userName,
                    'email'=>$data->email,
                    'phone'=>$data->phone,
                    'lineid'=>$data->lineid,
                    'Binusian'=>[
                        'jurusan'=>$data->jurusan,
                        'NIM'=>$data->NIM,
                        'flazz'=>$flazz,
                    ],
                    'role_id'=>$data->role_id,
                    'Payment'=>[
                        'id'=>$data->payment_id,
                       'name'=>$data->paymentName,
                       'image'=>'https://virtualconference-app.s3-ap-southeast-1.amazonaws.com/'.$data->image,
                       'status'=>$data->status,
                    ]
                ]);
            }
        }
        // dd($user);
        return $user;
    }

    public function getUserByID($userId)
    {
        return $this->user->find($userId);
    }
    public function updateUser($userId, $userData)
    {
        return $this->user->find($userId)->update($userData);
    }
    public function deleteUser($userId)
    {
        return $this->user->find($userId)->delete();
    }
    public function updateUserBinus($userId, $userData)
    {
        return $this->user->find($userId)->update($userData);
    }

}
