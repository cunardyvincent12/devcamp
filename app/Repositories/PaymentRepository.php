<?php

namespace App\Repositories;

use App\Payment;
use DB;

class PaymentRepository
{

    protected $payment;

    public function __construct(Payment $payment)
    {
        $this->payment = $payment;
    }

    public function getAllPayments()
    {
        return $this->payment->all('id', 'name', 'image');
    }

    public function getPaymentByID($paymentID)
    {
        return $this->payment->find($paymentID);
    }

    public function deletePayment($paymentID)
    {
        return $this->payment->find($paymentID)->delete();
    }

    public function updatePaymentData($paymentID, array $attributes)
    {
        return $this->payment->find($paymentID)->update($attributes);
    }

    public function createPayment(array $attributes)
    {
        return $this->payment->create($attributes);
    }
}
