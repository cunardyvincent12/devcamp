<?php

namespace App\Repositories;

use App\SendMessage;
use Illuminate\Support\Facades\DB;

class SendMessageRepository{
    protected $message;

    public function __construct(SendMessage $message)
    {
        $this->message = $message;
    }

    public function getAllMessages()
    {
        return $this->message->all('id', 'name', 'email','subject','message');
    }
    public function createMessage($messageData)
    {
        return $this->message->create($messageData);
    }
}