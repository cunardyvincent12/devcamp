<?php

namespace App\Repositories;

use App\User;
use Illuminate\Support\Facades\DB as FacadesDB;

class AuthRepository
{

    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function getUserByID($userID)
    {
        if(auth()->user()->payment_id == NULL){
            $userData = $this->user->where('id',$userID)
            ->select('name','email','phone','lineid','jurusan','NIM','flazz','payment_id','role_id')
            ->get();
            return $userData->transform(function($item){
                return [
                    'name'=>$item->name,
                    'email'=>$item->email,
                    'phone'=>$item->phone,
                    'lineid'=>$item->lineid,
                    'Binusian'=>[
                        'jurusan'=>$item->jurusan,
                        'NIM'=>$item->NIM,
                        'flazz'=>$item->flazz,
                    ],
                    'payment_id'=>$item->payment_id,
                    'role_id'=>$item->role_id,
                ];
            });
        }
        $userData = FacadesDB::table('users')->join('payments', 'users.payment_id', '=', 'payments.id')
        ->select('users.name','email','phone','lineid','jurusan','NIM','users.flazz','users.role_id','payment_id','payments.name as paymentName','payments.image','payments.status')
        ->where('users.id',$userID)
        ->get();
        return $userData->transform(function($item){
            return [
                'name'=>$item->name,
                'email'=>$item->email,
                'phone'=>$item->phone,
                'lineid'=>$item->lineid,
                'Binusian'=>[
                    'jurusan'=>$item->jurusan,
                    'NIM'=>$item->NIM,
                    'flazz'=>$item->flazz,
                ],
                'role_id'=>$item->role_id,
                'payment_id'=>$item->payment_id,
                'Payment' =>[
                    'name'=>$item->name,
                    'image'=>$item->image,
                    'status'=>$item->status,
                ]
            ];
        });
    }

    public function deleteUser($userID)
    {
        return $this->user->find($userID)->delete();
    }

    public function createUser(array $attributes)
    {
        return $this->user->create($attributes);
    }
}
