<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SendMessage extends Model
{
    protected $fillable =[
        'name','email','subject','message'
    ];
}
