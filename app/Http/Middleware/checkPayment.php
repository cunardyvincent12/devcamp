<?php

namespace App\Http\Middleware;

use App\Payment;
use Closure;

class checkPayment
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check() == true && auth()->user()->payment_id != NULL && Payment::where('id',auth()->user()->payment_id)->value('status') == 1 ){
            return $next($request);
        }
        abort(404);
    }
}
