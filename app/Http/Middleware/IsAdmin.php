<?php

namespace App\Http\Middleware;

use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // dd(auth()->check());
        if(auth()->check() == true && auth()->user()->role_id == 1 ){
            return $next($request);
        }
        abort(403);
    }
}
