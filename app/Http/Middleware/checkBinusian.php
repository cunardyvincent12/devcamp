<?php

namespace App\Http\Middleware;

use Closure;

class checkBinusian
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(auth()->check() == false && auth()->user()->role_id == 3 ){
            return $next($request);
        }
        abort(403);
    }
}
