<?php

namespace App\Http\Responses;

use Illuminate\Contracts\Support\Responsable;
use App\Http\Responses\ResponseMessage;

class BaseResponse implements Responsable
{

    protected $status;
    protected $message;

    public function __construct($data, $status)
    {
        $this->data = $data;
        $this->status = $status;
        // dd($data);
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function toResponse($request)
    {
        return response()->json([
            
            'status' => $this->status,
            'message' => $this->data,
            'error' => '',
        ]);
    }
}
