<?php

namespace App\Http\Responses;

use App\Http\Responses\BaseResponse;
use App\Http\Responses\ResponseMessage;

class DataResponse extends BaseResponse
{

    protected $data;

    public function __construct($data, $status)
    {
        parent::__construct($data, $status);
        $this->data = $data;
    }

    public function toResponse($request)
    {
        return response()->json([
            'status' => $this->status,
            'message' => ResponseMessage::MESSAGE_SUCCESS,
            'error' => '',
            'data' => $this->data
        ]);
    }
}
