<?php

namespace App\Http\Responses;

class ResponseCode
{

    const SUCCESS_CODE = 200;
    const UNAUTHORIZED_CODE = 403;
    const UNAUTHENTICATED_CODE = 401;
    const CREATED_CODE = 201;
    const UNPROCESSABLE_ENTITY = 422;
}
