<?php

namespace App\Http\Responses;

class ResponseMessage
{
    
    const MESSAGE_SUCCESS = 'Success';
    const MESSAGE_UNAUTHORIZED = 'Unauthorized';
    const MESSAGE_UNAUTHENTICATED = 'Unauthenticated';
}
