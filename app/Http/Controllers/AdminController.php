<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserDataRequest;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\ResponseCode;
use App\Http\Responses\DataResponse;
use App\Services\PaymentService;
use App\Services\UserDataService;
use Illuminate\Support\Facades\Password;

class AdminController extends Controller
{
    protected $userService;

    public function __construct(UserDataService $userService, PaymentService $paymentService)
    {
        $this->userService = $userService;
        $this->paymentService = $paymentService;
        $this->middleware('admin');
    }

    public function getAllUser(Request $request)
    {
        $data = $this->userService->getAllUsers($request);
        return new DataResponse($data, ResponseCode::SUCCESS_CODE);
    }

    public function updateUser(Request $request, $userID)
    {
        $data = $this->userService->updateUser($userID, $request);
        return new DataResponse($data, ResponseCode::SUCCESS_CODE);
    }

    public function deleteUser($userID)
    {
        $data = $this->userService->deleteUser($userID);
        return new DataResponse($data, ResponseCode::SUCCESS_CODE);
    }

    public function getUserByID($userID)
    {
        $data = $this->userService->getUserByID($userID);
        return new DataResponse($data, ResponseCode::SUCCESS_CODE);
    }

    public function verifyBinusian($userID){
        $data = $this->userService->verifiedBinusian($userID);
        return new DataResponse($data, ResponseCode::SUCCESS_CODE);
    }

    public function verifyPayment($userID){
        $data = $this->paymentService->verfiedPayment($userID);
        return new DataResponse($data, ResponseCode::SUCCESS_CODE);
    }
    public function forgot() {
        $credentials = request()->validate(['email' => 'required|email']);

        Password::sendResetLink($credentials);

        return response()->json(["msg" => 'Reset password link sent on your email id.']);
    }
    public function reset() {
        $credentials = request()->validate([
            'email' => 'required|email',
            'token' => 'required|string',
            'password' => 'required|string|confirmed'
        ]);

        $reset_password_status = Password::reset($credentials, function ($user, $password) {
            $user->password = bcrypt($password);
            $user->save();
        });

        if ($reset_password_status == Password::INVALID_TOKEN) {
            return response()->json(["msg" => "Invalid token provided"], 400);
        }

        return response()->json(["msg" => "Password has been successfully changed"]);
    }
}
