<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\PaymentService;
use App\Http\Requests\PaymentRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\ResponseCode;
use App\Http\Responses\DataResponse;
use Symfony\Component\Mime\Message;

class PaymentController extends Controller
{
    protected $paymentService;

    public function __construct(PaymentService $paymentService)
    {
        $this->paymentService = $paymentService;
    }

    public function getAllPayment(Request $request)
    {
        $data = $this->paymentService->getAllPayments($request);
        return new DataResponse($data, ResponseCode::SUCCESS_CODE);
    }

    public function updatePayment($paymentID, PaymentRequest $request)
    {

        $data = $this->paymentService->updatePayment($paymentID, $request);
        return new DataResponse($data, $data->status());
    }

    public function createPayment(PaymentRequest $request)
    {
        $data = $this->paymentService->createPayment($request);
        return new DataResponse($data,ResponseCode::CREATED_CODE );
    }

    public function deletePayment($paymentID)
    {
        $data = $this->paymentService->deletePayment($paymentID);
        return new DataResponse($data,ResponseCode::SUCCESS_CODE);
    }

    public function getPaymentByID()
    {
        $paymentID = auth()->user()->id;
        $data = $this->paymentService->getPaymentByID($paymentID);
        return new DataResponse($data, ResponseCode::SUCCESS_CODE);
    }

}
