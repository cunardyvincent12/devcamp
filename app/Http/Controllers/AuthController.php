<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use App\User;
use App\Services\AuthenticateService;
use App\Services\UserDataService;
use App\Http\Requests\UserDataRequest;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\ResponseCode;
use App\Http\Responses\DataResponse;


class AuthController extends Controller {

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct(AuthenticateService $authService, UserDataService $userDataService) {
        $this->authService = $authService;
        $this->userDataService = $userDataService;
        $this->middleware('auth:api', ['except' => ['login', 'register']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
        $data = $this->authService->login($request);
        return new DataResponse($data, $data->status());
    }

    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(UserDataRequest $request) {
        $data = $this->authService->register($request);
        return new DataResponse($data, ResponseCode::SUCCESS_CODE);
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        $data = $this->authService->logout();
        return new BaseResponse($data, ResponseCode::SUCCESS_CODE);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        $data = $this->authService->refresh();
        return new DataResponse($data, ResponseCode::SUCCESS_CODE);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile(Request $request) {
        $data = $this->authService->userProfile($request);
        return new DataResponse($data, $data->status());
    }

    public function getUserById(Request $request, $userID){
        $data = $this->authService->getUserById($userID, $request);
        return new DataResponse($data, ResponseCode::SUCCESS_CODE);
    }

    public function inputNim(Request $request)
    {
        $data = $this->userDataService->updateUserBinus($request);
        return new BaseResponse($data, ResponseCode::SUCCESS_CODE);
    }

    public function updateFlazz(Request $request, $userID)
    {
        $data = $this->authService->updateFlazz($userID, $request);
        return new DataResponse($data, ResponseCode::SUCCESS_CODE);
    }
}
