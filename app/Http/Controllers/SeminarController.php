<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Seminar;
use App\Http\Responses\BaseResponse;
use App\Http\Responses\ResponseCode;
use App\Http\Responses\DataResponse;
use App\Http\Requests\ScheduleRequest;
use App\Services\SeminarService;

class SeminarController extends Controller
{
    protected $seminarService;

    public function __construct(SeminarService $seminarService){
        $this->seminarService = $seminarService;
    }

    public function getAllSeminars(Request $request)
    {
        $data = $this->seminarService->getAllSeminars($request);
        return new DataResponse($data, ResponseCode::SUCCESS_CODE);
    }

    public function createSeminar(ScheduleRequest $request)
    {
        $data = $this->seminarService->createSeminar($request);
        return new DataResponse($data, $data->status());
    }

    public function updateSeminar(ScheduleRequest $request, $seminarID)
    {
        $data = $this->seminarService->updateSeminar($request, $seminarID);
        return new DataResponse($data, $data->status());
    }

    public function deleteSeminar($seminarID)
    {
        $data = $this->seminarService->deleteSeminar($seminarID);
        return new DataResponse($data, ResponseCode::SUCCESS_CODE);
    }
    public function sendMail()
    {
        return $this->seminarService->sendMail();
    }
}
