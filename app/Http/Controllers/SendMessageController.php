<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\SendMessageService;
use App\Http\Requests\SendMessageRequest;
use App\Http\Responses\ResponseCode;
use App\Http\Responses\DataResponse;

class SendMessageController extends Controller
{
    protected $messageService;

    public function __construct(SendMessageService $messageService)
    {
        $this->messageService = $messageService;
    }

    public function getAllMessages(Request $request)
    {
        $data = $this->messageService->getAllMessages($request);
        return new DataResponse($data, ResponseCode::SUCCESS_CODE);
    }

    public function createMessage(SendMessageRequest $request)
    {
        $data = $this->messageService->createmessage($request);
        return new DataResponse($data,ResponseCode::CREATED_CODE);
    }
}
