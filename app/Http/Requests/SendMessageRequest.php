<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'required',
            'email' => 'required|email:rfc,dns,filter',
            'subject' => 'required',
            'message' => 'required|max:300',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name must be Inputed',
            'email.required' => 'Email must be Inputed',
            'email.email' => 'This is inccorect type of email.',
            'subject.required' => 'Subject must be Inputed',
            'message.required' => 'Message must be Inputed',
            'message.max' => 'Message will contain 300 character or less',
        ];
    }
}
