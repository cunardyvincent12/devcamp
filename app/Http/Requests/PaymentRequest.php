<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'image'=>'required|file|mimes:jpeg,jpg,png,pdf|max:5120'
        ];
    }
    public function messages()
    {
        return [
            'image.required' => ' Payment must be uploaded',
            'image.mimes' => ' Payment file format must .jpeg, .jpg, .png, or .pdf',
            'image.max' => ' Payment file must be under 5 MB'
        ];
    }
}
