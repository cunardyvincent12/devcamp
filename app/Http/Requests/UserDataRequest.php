<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Symfony\Component\Console\Input\Input;

class UserDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function __construct(Request $request)
    {
        $this->request =$request;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        // dd($this->request->has('role')->get());
        if($this->request->has('role'))
        {
            if($this->request->get('role') == "true")
            {
                return [
                    'name' => 'required|string|between:2,100',
                    'email' => 'required|string|email:rfc,dns,filter|max:100|unique:users',
                    'password' => 'required|string|min:6',
                    'phone' => 'required|numeric',
                    'lineid' => 'required|string',
                    'flazz' => 'required|file|mimes:jpeg,jpg,png,pdf|max:5120'
                    ];

            }
        }
            return [
                'name' => 'required|string|between:2,100',
                'email' => 'required|string|email:rfc,dns|max:100|unique:users',
                'password' => 'required|string|min:6',
                'phone' => 'required|numeric',
                'lineid' => 'required|string',
                ];

    }
    public function messages()
    {
        return [
            'name.required' => 'Name must be Inputed',
            'email.required' => 'Email must be Inputed',
            'email.email' => 'This is inccorect type of email.',
            'email.unique' => 'This email had been used.',
            'phone.required' => 'Phone must be Inputed',
            'phone.numeric' => 'Phone must in digits',
            'lineid.required' => 'LINE ID must be Inputed',
            'role.required' => 'Please choose the corresponding option',
            'flazz.required' => ' Flazz must be uploaded',
            'flazz.mimes' => ' Flazz card image format must .jpeg, .jpg, .png, or .pdf',
            'flazz.max' => ' Flazz card Image must be under 5 MB'
        ];
    }
}
