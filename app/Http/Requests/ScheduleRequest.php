<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ScheduleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' =>'required',
            'speaker' =>'required',
            'date' =>'required|date|date_format:Y-m-d|after:today',
            'time' =>'required',
            'link' =>'required|url',
        ];
    }
    public function messages()
    {
        return [
            'title.required' => 'Title is required please input the data',
            'speaker.required' => 'Speaker is required please input the data',
            'date.required' => 'date is required please input the data',
            'date.date' => 'date must be in date format',
            'date.after' => 'date must not be the past date must be now or future',
            'time.required' => 'time is required please input the data',
            'link.required' => 'link is required please input the data',
            'link.url' => 'link must be url ex: https://bncc.net',
        ];
    }
}
