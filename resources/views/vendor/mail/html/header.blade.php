<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'TechnoScape')
<img src="{{url('/assets/logo-technoscape-virtualcon.png')}}" class="logo" alt="Technoscape Logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>
