
@component('mail::message')

# Reminder

As for the event is coming near we are here to remind our value participant

pleased for you to click link below to proceed

@component('mail::button', ['url' => 'https://virtualconference-app.bncc.net/login'])
Login
@endcomponent

Thanks,<br>
{{$data->name}}
@endcomponent
